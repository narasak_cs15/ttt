*** Settings ***
Resource          Center_Resource.txt

*** Keywords ***
Open iCore
    [Arguments]    ${URL}=https://miraidcp-uat.krungsri.net/
    Web.Open Browser    ${URL}

iCore Login
    [Arguments]    ${AccountAD}=70130131
    : FOR    ${Index}    IN RANGE    1    3
    \    Web.Cancel I Frame
    \    Web.I Frame    //*[@id='loginWindow']
    \    ${Status}    Web.Check Message    Welcome
    \    IF Conditions    ${Status}    !=    PASS    Web.Reload Page
    \    IF Conditions    ${Status}    !=    PASS    Exit For Loop
    Web.Cancel I Frame
    Web.I Frame    //*[@id='loginWindow']
    Web.Input Data    //*[@id='userid']    ${AccountAD}
    Web.Input Data    //*[@id='password']    P@ssw0rd
    Web.Manual Input    //*[@id='authCode']
    Web.Click    //*[@id='submit_btn']
    Web.Cancel I Frame
    Web.Click    //*[contains(@id,'popModalbtnConfirm')]

iCore Logout
    Web.Cancel I Frame
    Web.I Frame    //*[@id="coreWindow"]    //*[@id="gyinfo"]
    Web.Click    //*[@data-category="Main Navigation" and @id="close" and @data-event="Home" and @data-type="Quit"]
    Web.Cancel I Frame
    Web.Click    //*[contains(@id,'popModalbtnConfirm')]

iCore Menu [2601]Sub Account Enquiry
    [Arguments]    ${AccountNo}=3007035137
    Web.I Frame    //*[@id='coreWindow']    //*[@id="layout-center"]
    Web.Enter Input Data    //*[@id='form']    2601
    Web.Cancel I Frame
    Web.I Frame    //*[@id="coreWindow"]    //*[@id="layout-center"]    //*[@scrolling="yes"]
    Web.Enter Input Data    //*[@id="acct_no"]    ${AccountNo}
    Web.Cancel I Frame
    Web.I Frame    //*[@scrolling="yes"]
    Web.Double Click    //*[@id="1"]/td[6]
    Web.Cancel I Frame
    Web.I Frame    //*[@id="coreWindow"]    //*[@id="layout-center"]    //*[@scrolling="yes"]
    Web.Click    //*[@id="inquireconfirmbtn"]

iCore Menu [2603]Deposit Transaction Enquiry [Main Account]
    [Arguments]    ${AccountNo}=3007035137
    Web.I Frame    //*[@id='coreWindow']    //*[@id="layout-center"]
    Web.Enter Input Data    //*[@id='form']    2601
    Web.Cancel I Frame
    Web.I Frame    //*[@id="coreWindow"]    //*[@id="layout-center"]    //*[@scrolling="yes"]
    Web.Enter Input Data    //*[@id="acct_no"]    ${AccountNo}
    Web.Cancel I Frame
    Web.I Frame    //*[@scrolling="yes"]
    Web.Double Click    //*[@id="1"]/td[6]
    Web.Cancel I Frame
    Web.I Frame    //*[@id="coreWindow"]    //*[@id="layout-center"]    //*[@scrolling="yes"]
    Web.Click    //*[@id="inquireconfirmbtn"]

iCore Menu [2603]Deposit Transaction Enquiry [Smart Account]
    [Arguments]    ${AccountNo}=3007035137
    Web.I Frame    //*[@id='coreWindow']    //*[@id="layout-center"]
    Web.Enter Input Data    //*[@id='form']    2601
    Web.Cancel I Frame
    Web.I Frame    //*[@id="coreWindow"]    //*[@id="layout-center"]    //*[@scrolling="yes"]
    Web.Enter Input Data    //*[@id="acct_no"]    ${AccountNo}
    Web.Cancel I Frame
    Web.I Frame    //*[@scrolling="yes"]
    Web.Double Click    //*[@id="2"]/td[6]
    Web.Cancel I Frame
    Web.I Frame    //*[@id="coreWindow"]    //*[@id="layout-center"]    //*[@scrolling="yes"]
    Web.Click    //*[@id="inquireconfirmbtn"]

iCore Menu [2201]Manual Adjustment [D-Debit]
    [Arguments]    ${AccountNo}=3007035137    ${Amount}=10000
    Web.Cancel I Frame
    Web.I Frame    //*[@id='coreWindow']    //*[@id="layout-center"]
    Web.Enter Input Data    //*[@id='form']    2201
    Web.Cancel I Frame
    Web.I Frame    //*[@id="coreWindow"]    //*[@id="layout-center"]    //*[@scrolling="yes"]
    Web.Input Data    //*[@id="acct_no1"]    ${AccountNo}
    Web.Select Dorp Down    //*[@id="acct_no2"]    INTER_DEPARTMENT_TR-Inter Department (TR)
    Web.Input Data    //*[@id="trxn_amt"]    ${Amount}
    Web.Select Dorp Down    //*[@id="bal_type"]    D-Debit
    Web.Click    //*[@id="btnConfirm"]

iCore Menu [2201]Manual Adjustment [C-Credit]
    [Arguments]    ${AccountNo}=3007035137    ${Amount}=10000
    Web.Cancel I Frame
    Web.I Frame    //*[@id='coreWindow']    //*[@id="layout-center"]
    Web.Enter Input Data    //*[@id='form']    2201
    Web.Cancel I Frame
    Web.I Frame    //*[@id="coreWindow"]    //*[@id="layout-center"]    //*[@scrolling="yes"]
    Web.Input Data    //*[@id="acct_no1"]    ${AccountNo}
    Web.Select Dorp Down    //*[@id="acct_no2"]    INTER_DEPARTMENT_TR-Inter Department (TR)
    Web.Input Data    //*[@id="trxn_amt"]    ${Amount}
    Web.Select Dorp Down    //*[@id="bal_type"]    C-Credit
    Web.Click    //*[@id="btnConfirm"]
