*** Settings ***
Resource          Center_Resource.txt

*** Keywords ***
Open Connection RM
    [Arguments]    ${Hostname}=172.16.129.254    ${AccountAD}=11255841    ${Password}=qwerty03
    Mainframe.Connection Maimframe    ${Hostname}
    Mainframe.Compare Message    K R U N G S R I
    Mainframe.Enter Send Message    log acicterm
    Mainframe.Clear Screen
    Mainframe.Enter Send Message    CESN
    Mainframe.Send Message    ${AccountAD}
    Mainframe.Tab
    Mainframe.Enter Send Message    ${Password}
    Mainframe.Enter Send Message    ts01svmn

RM [1. INFORMATION INQUIRY]
    [Arguments]    ${AccuontNo}=8001053113
    Mainframe.Enter Send Message    x
    Mainframe.Send Message    ${AccuontNo}
    Mainframe.Enter Send Message    x

RM [2. BALANCE INQUIRY]
    [Arguments]    ${AccuontNo}=8001053113
    Mainframe.Enter Send Message    x
    Mainframe.Send Message    ${AccuontNo}
    Mainframe.Tab
    Mainframe.Enter Send Message    x

RM [3. STATEMENT INQUIRY]
    [Arguments]    ${AccuontNo}=8001053113
    Mainframe.Enter Send Message    x
    Mainframe.Send Message    ${AccuontNo}
    Mainframe.Tab
    Mainframe.Tab
    Mainframe.Enter Send Message    x

RM [4. TODAY TRANSACTION INQUIRY]
    [Arguments]    ${AccuontNo}=8001053113
    Mainframe.Enter Send Message    x
    Mainframe.Send Message    ${AccuontNo}
    Mainframe.Tab
    Mainframe.Tab
    Mainframe.Tab
    Mainframe.Enter Send Message    x

RM [5. CLOSING BALANCE INQUIRY]
    [Arguments]    ${AccuontNo}=8001053113
    Mainframe.Enter Send Message    x
    Mainframe.Send Message    ${AccuontNo}
    : FOR    ${Index}    IN RANGE    4
    \    Mainframe.Tab
    Mainframe.Enter Send Message    x

RM [6. UNPOST TRANSACTION INQUIRY]
    [Arguments]    ${AccuontNo}=8001053113
    Mainframe.Enter Send Message    x
    Mainframe.Send Message    ${AccuontNo}
    : FOR    ${Index}    IN RANGE    5
    \    Mainframe.Tab
    Mainframe.Enter Send Message    x

RM [7. HOLD INFORMATION INQUIRY]
    [Arguments]    ${AccuontNo}=8001053113
    Mainframe.Enter Send Message    x
    Mainframe.Send Message    ${AccuontNo}
    : FOR    ${Index}    IN RANGE    6
    \    Mainframe.Tab
    Mainframe.Enter Send Message    x

RM [8. TODAY POSTED INQUIRY]
    [Arguments]    ${AccuontNo}=8001053113
    Mainframe.Enter Send Message    x
    Mainframe.Send Message    ${AccuontNo}
    : FOR    ${Index}    IN RANGE    7
    \    Mainframe.Tab
    Mainframe.Enter Send Message    x

RM [9. ACCOUNT CLOSED INQUIRY]
    [Arguments]    ${AccuontNo}=8001053113
    Mainframe.Enter Send Message    x
    Mainframe.Send Message    ${AccuontNo}
    : FOR    ${Index}    IN RANGE    8
    \    Mainframe.Tab
    Mainframe.Enter Send Message    x

RM [10. TELLER AND REFERENCE INQUIRY]
    [Arguments]    ${AccuontNo}=8001053113
    Mainframe.Enter Send Message    x
    Mainframe.Send Message    ${AccuontNo}
    : FOR    ${Index}    IN RANGE    9
    \    Mainframe.Tab
    Mainframe.Enter Send Message    x

RM [11. CONVERT BRANCH INQUIRY]
    [Arguments]    ${AccuontNo}=8001053113
    Mainframe.Enter Send Message    x
    Mainframe.Send Message    ${AccuontNo}
    : FOR    ${Index}    IN RANGE    10
    \    Mainframe.Tab
    Mainframe.Enter Send Message    x

RM [12. LOST PASSBOOK INQUIRY]
    [Arguments]    ${AccuontNo}=8001053113
    Mainframe.Enter Send Message    x
    Mainframe.Send Message    ${AccuontNo}
    : FOR    ${Index}    IN RANGE    11
    \    Mainframe.Tab
    Mainframe.Enter Send Message    x

RM [13. เงื่อนไขการสั่งจ่าย]
    [Arguments]    ${AccuontNo}=8001053113
    Mainframe.Enter Send Message    x
    Mainframe.Send Message    ${AccuontNo}
    : FOR    ${Index}    IN RANGE    12
    \    Mainframe.Tab
    Mainframe.Enter Send Message    x

RM [14. PASSBOOK NUMBER INQUIRY]
    [Arguments]    ${AccuontNo}=8001053113
    Mainframe.Enter Send Message    x
    Mainframe.Send Message    ${AccuontNo}
    : FOR    ${Index}    IN RANGE    13
    \    Mainframe.Tab
    Mainframe.Enter Send Message    x

RM [15. ACCOUNT CONVERT BRANCH INQUIRY]
    [Arguments]    ${AccuontNo}=8001053113
    Mainframe.Enter Send Message    x
    Mainframe.Send Message    ${AccuontNo}
    : FOR    ${Index}    IN RANGE    14
    \    Mainframe.Tab
    Mainframe.Enter Send Message    x

RM [16. SAMEDAY FLOAT TRANSACTION]
    [Arguments]    ${AccuontNo}=8001053113
    Mainframe.Enter Send Message    x
    Mainframe.Send Message    ${AccuontNo}
    : FOR    ${Index}    IN RANGE    15
    \    Mainframe.Tab
    Mainframe.Enter Send Message    x

RM [17. HOLD LOG INQUIRY]
    [Arguments]    ${AccuontNo}=8001053113
    Mainframe.Enter Send Message    x
    Mainframe.Send Message    ${AccuontNo}
    : FOR    ${Index}    IN RANGE    16
    \    Mainframe.Tab
    Mainframe.Enter Send Message    x
