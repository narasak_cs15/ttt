*** Settings ***
Resource          Center_Resource.txt

*** Keywords ***
Open Mirai App Backend
    Web.Open Browser    https://miraimim-uat.krungsri.net/mirai_webs/#/login

Mirai App Backend [Login]
    Web.Input Data    //*[@*='username']    admin
    Web.Input Data    //*[@*='password']    123456
    Web.Manual Input    //*[@*='randomcode']
    Web.Click    //*[@*='button']

Mirai App Backend [App Infomation]
    Web.Click    //*[*='App Infomation']

Mirai App Backend [History Record]
    Web.Click    //*[*='History Record']

Mirai App Backend [History Record Get Data]
    : FOR    ${Index}    IN RANGE    1    22
    \    ${GetTable}    Get Table View    //*[@*='el-table__body']    ${Index}    3
    \    Web.Click    //tr[${Index}]/td[4]/div/button
    \    Web.Click    //*[*='Request']//*[@*='el-dialog__close el-icon el-icon-close']
    \    Web.Click    //tr[${Index}]/td[5]/div/button
    \    Web.Click    //*[*='Response']//*[@*='el-dialog__close el-icon el-icon-close']
    \    Log To Console    ${GetTable}

Mirai App Backend [Login info]
    Web.Click    //li[*='Login info']

Mirai App Backend [Login info Get Data]
    : FOR    ${Index}    IN RANGE    1    22
    \    ${GetTable}    Get Table View    //*[@*='el-table__body']    ${Index}    4
    \    Web.Click    //tr[${Index}]/td[5]/div/button
    \    Web.Click    //*[*='User Login Detail']//*[@*='el-dialog__close el-icon el-icon-close']
    \    Log To Console    ${GetTable}

Mirai App Backend [Logout]
    Web.Click    //*[@*='user-avatar']
