*** Settings ***
Resource          Center_Resource.txt

*** Keywords ***
Open mobile sumsung A8
    [Arguments]    ${Platform}=Android    ${Version}=7.1.1    ${DevicesName}=52003172fed02577
    Mobile.Open Application    ${Platform}    ${Version}    ${DevicesName}    com.sunline.Mirai    com.example.hellotiny.SplashActivity

Switch language is EN
    ${Return}    Mobile.Get Message    เข้าสู่ระบบ
    IF Conditions    ${Return}    ==    PASS    Mobile.Click    //*[@content-desc='en']

Switch language is TH
    ${Return}    Mobile.Check Message    Login
    IF Conditions    ${Return}    ==    PASS    Mobile.Click    //*[@content-desc='th']

Click Sign Up
    Mobile.Click    //*[@content-desc="SignUp"]

Click login app
    Mobile.Click    //*[@content-desc="Login"]

Click need help click here
    Mobile.Click    //*[@content-desc="text_1"]

PIN Confirm
    [Arguments]    ${PIN}=147258
    @{List}    Split String To Characters    ${PIN}
    : FOR    ${Index}    IN    @{List}
    \    Mobile.Click    //*[@content-desc='${Index}']

Forget PIN
    Mobile.Go Back
    Mobile.Click    //*[@content-desc='forgetBtn']

Click menu more
    Mobile.Click    //*[@content-desc='More']

Click menu transfer
    Mobile.Click    //*[@content-desc='Transfer']

Click menu QR
    Mobile.Click    //*[@content-desc='PromPay']

Click menu notification
    Mobile.Click    //*[@content-desc='Notification']

Click menu dashboard
    Mobile.Click    //*[@content-desc='Dashboard']

Select kept savings
    Mobile.Click    //*[@content-desc='subAcctName']
    Mobile.Check Element    //*[@content-desc='img_1']

Select grow savings
    Mobile.Click    //*[@content-desc='smartSubAcctName']
    Mobile.Check Element    //*[@content-desc='img_3']

Select fun savings
    Mobile.Click    //*[@content-desc='piggySubAcctName']
    Mobile.Check Element    //*[@content-desc='img_3']

Click transfer to bank
    Mobile.Click    //*[@content-desc='label1']
    Mobile.Swipe    //*[@content-desc='label1']    //*[@content-desc='label_title']

Click bank Krungsri
    Mobile.Click    //*[@content-desc='label_list_1_025']

Click bank BBL
    Mobile.Click    //*[@content-desc='label_list_1_002']

Click bank KTB
    Mobile.Click    //*[@content-desc='label_list_1_006']

Click bank KBANK
    Mobile.Click    //*[@content-desc='label_list_1_004']

Click bank SCB
    Mobile.Click    //*[@content-desc='label_list_1_014']

Click bank TMB
    Mobile.Click    //*[@content-desc='label_list_1_011']

Click bank TBANK
    Mobile.Click    //*[@content-desc='label_list_1_065']

Click bank KK
    Mobile.Click    //*[@content-desc='label_list_1_069']

Click bank GHB
    Mobile.Click    //*[@content-desc='label_list_1_033']

Click bank GSB
    Mobile.Click    //*[@content-desc='label_list_1_030']

Click bank BAAC
    Mobile.Click    //*[@content-desc='label_list_1_034']

Click bank CITI
    Mobile.Click    //*[@content-desc='label_list_1_017']

Click bank UOB
    Mobile.Click    //*[@content-desc='label_list_1_024']

Click bank CIMB
    Mobile.Click    //*[@content-desc='label_list_1_022']

Click bank SCBT
    Mobile.Click    //*[@content-desc='label_list_1_020']

Click bank TSCO
    Mobile.Click    //*[@content-desc='label_list_1_067']

Click bank TCRB
    Mobile.Click    //*[@content-desc='label_list_1_071']

Click bank MHCB
    Mobile.Click    //*[@content-desc='label_list_1_039']

Click bank SMBC
    Mobile.Click    //*[@content-desc='label_list_1_018']

Click bank ICBC
    Mobile.Click    //*[@content-desc='label_list_1_070']

Click bank HSBC
    Mobile.Click    //*[@content-desc='label_list_1_031']

Click transfer to promptpay
    Mobile.Click    //*[@content-desc='label2']
    Mobile.Swipe    //*[@content-desc='label2']    //*[@content-desc='label_title']

Input promptpay
    [Arguments]    ${Message}
    Mobile.Click    //*[@id='et_left']
    @{List}    Get Regexp Matches    ${Message}    (.)    1
    : FOR    ${Index}    IN    @{List}
    \    Mobile.Click    //*[@id='input_number_${Index}']
    Mobile.Click    //*[@id='input_done']

Select account from contact
    [Arguments]    ${Message}=0903696670
    Mobile.Click    //*[@content-desc='label_3']
    Run Keyword And Ignore Error    Mobile.Click    //*[@txt="ALLOW"]
    Mobile.Input Data    //*[@resource-id='android:id/search_src_text']    ${Message}
    Mobile.Click    //*[@resource-id='com.samsung.android.contacts:id/cliv_name_textview']

Transfer page [input AccountNo]
    [Arguments]    ${Account}=3007035015
    Mobile.Input Data    //*[@*='Account No']//*[@*='com.sunline.Mirai:id/et_left']    ${Account}

Transfer page [input Amount]
    [Arguments]    ${Amount}=1.00
    Mobile.Input Data    //*[@*='Amount']//*[@*='com.sunline.Mirai:id/et_left']    ${Amount}

Transfer page [Note]
    [Arguments]    ${Note}=RF Note
    Mobile.Input Data    //*[@*='Note']//*[@*='com.sunline.Mirai:id/et_left']    ${Note}

Transfer page [Next Button]
    Mobile.Click    //*[@content-desc='button_transfer']

Transfer Confirm page [From Name]
    Mobile.Get Message Element    //*[@*="name_from"]

Transfer Confirm page [From Bank Name]
    Mobile.Get Message Element    //*[@*="fromBank"]

Transfer Confirm page [From AccNo]
    Mobile.Get Message Element    //*[@*="acct_from"]

Transfer Confirm page [To Name]
    Mobile.Get Message Element    //*[@*="name_to"]

Transfer Confirm page [To Bank Name]
    Mobile.Get Message Element    //*[@*="toBank"]

Transfer Confirm page [To AccNo]
    Mobile.Get Message Element    //*[@*="acct_to"]

Transfer Confirm page [Amount]
    Mobile.Get Message Element    //*[@*="amount"]

Transfer Confirm page [Fee]
    Mobile.Get Message Element    //*[@*="fee"]

Transfer Confirm page [Note]
    Mobile.Get Message Element    //*[@*="note"]

Transfer Confirm page [Confirm Button]
    Mobile.Get Message Element    //*[@*="button_next"]
    Comment    Mobile.Click    //*[@*="button_next"]
